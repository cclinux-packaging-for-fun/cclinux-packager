Name:           cclinux-packager
Version:        0.0.1
Release:        1%{?dist}
Summary:        Tools and files necessary for building Circle Linux packages
Group:          Applications/Productivity

License:        GPLv3+
URL:            https://git.cclinux.org/cclinux-packager
Source0:        cc-koji.conf
Source1:        COPYING

Requires:       rpm-build rpmdevtools rpmlint
Requires:       redhat-rpm-config

# What other packages are required?
# Requires:       koji mock

BuildArch:      noarch

%description
Tools to help set up a Circle Linux packaging environment and interact with the
Community Build System (Koji).

%prep

%build
cp %{SOURCE0} .
cp %{SOURCE1} .

%install
conf_dir=%{buildroot}%{_sysconfdir}/koji.conf.d
mkdir -p ${conf_dir}
install -p -m 0644 cc-koji.conf ${conf_dir}/cc-koji.conf

%files
%license COPYING
%config(noreplace) %{_sysconfdir}/koji.conf.d/cc-koji.conf

%changelog
* Sun Mar 27 2022 Chenxiong Qi <qcxhome@gmail.com> - 0.0.1-1
- initial build
